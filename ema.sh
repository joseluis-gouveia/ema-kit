#!/usr/bin/env bash

#PATH=`pwd -P ../`
#current_folder=$(dirname "$0")
# mkdir -p ../$current_folder/src/{assets/{img/{images,contents},js/components,scss/{components,pages,partials,structure},fonts},views/{pages,partials,layouts,helpers}}
# echo "Estrutura Criada"
ROOT="$(dirname "$(dirname $PWD)")"

# while true; do
#     read -p "Usar default template? (Sim/Não) " sn
#     case $sn in
#         [Ss]* ) cp -rv $current_folder/default/ ../$current_folder; break;;
#         [Nn]* ) break;;
#         * ) echo "Preciso de uma resposta!";;
#     esac
# done

# echo "Template Copiado."

GLOBAL="npm i -g npm gulp";
INSTALL_NPM="npm cache clean; npm i --production --silent";
INSTALL_NODE="node structure.js";
PERMISSION="chown -R $(whoami) $ROOT";

RED='\033[0;31m'
NC='\033[0m' # No Color

while getopts db option
do
    case "${option}"
    in
        b) $INSTALL_NPM; $INSTALL_NODE; echo "Estrutura e Dependencias Instaladas!"; exit;;
        d) sudo $GLOBAL; $INSTALL_NPM; $INSTALL_NODE; echo "Instalação Efectuada!"; exit;;
    esac
done

printf "(${RED}f${NC}ix/${RED}u${NC}pdate/${RED}b${NC}uild/${RED}g${NC}/${RED}p${NC}ackages/${RED}s${NC}ubmodule/${RED}no${NC})\n"

while true; do
    read -p "Instalar EMA Kit? " input
    case $input in
        g ) $GLOBAL; echo "Instalação Global!"; break;;
        b ) $INSTALL_NPM; $INSTALL_NODE; echo "Estrutura e Dependencias Instaladas!"; break;;
        u ) $INSTALL_NODE; echo "Config Actualizado!"; break;;
        f ) sudo $PERMISSION echo "Permissões Alteradas em "$ROOT; break;;
        p ) sudo npm i -g gulp; npm update; echo "Packages Actualizados!"; break;;
        no ) echo "Cancelado!"; break;;
        * ) echo "Preciso de uma resposta!";;
    esac
done


exit;