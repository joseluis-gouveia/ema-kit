<?php
$host = $_SERVER['HTTP_HOST'];
$remoteAddr = $_SERVER['REMOTE_ADDR'];

// Check for local development environment
if (strpos($host, "localhost") !== false || substr($remoteAddr, 0, 8) == "192.168.") {
    define("BDSERVER", DEVHOST);
    define("BDUSERNAME", DEVUSERNAME);
    define("BDPASSWORD", DEVPASSWORD);
    define("BDBD", DEVDATABASE);
}
// Check for the 'dev.' subdomain
else if (strpos($host, "dev.") === 0) {
    define("BDSERVER", DEVONLINEHOST);
    define("BDUSERNAME", DEVONLINEUSERNAME);
    define("BDPASSWORD", DEVONLINEPASSWORD);
    define("BDBD", DEVONLINEDATABASE);
}
// For all other environments
else {
    define("BDSERVER", CONFIGHOST);
    define("BDUSERNAME", CONFIGUSERNAME);
    define("BDPASSWORD", CONFIGPASSWORD);
    define("BDBD", CONFIGDATABASE);
}
